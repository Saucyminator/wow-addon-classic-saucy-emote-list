## Interface: 11302
## Title: Saucy Emote List
## Notes: Some note
## Version: v0.1.0
## Author: Saucy (|cffffffffNelaesia|r @ Zandalar Tribe EU)
## DefaultState: Enabled
## SavedVariables: SaucyDBSEL

lib\LibStub\LibStub.lua
lib\CallbackHandler-1.0\CallbackHandler-1.0.lua
lib\AceAddon-3.0\AceAddon-3.0.xml
lib\AceGUI-3.0\AceGUI-3.0.xml
lib\LibSharedMedia-3.0\lib.xml
lib\AceGUI-3.0-SharedMediaWidgets\widget.xml
lib\LibDataBroker-1.1\LibDataBroker-1.1.lua
lib\LibDBIcon-1.0\lib.xml

core\init.lua

locale\enUS.lua

core\config.lua
core\core.lua
