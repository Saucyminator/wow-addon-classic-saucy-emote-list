-- TODO: Add: Window can locked
-- TODO: Add: Debug window with buttons to test different inputs, like /SEL, /SEL help, etc...
-- TODO: change cmd to /sel debug on/off, use /sel debug as a debug-window open/close?


-- init
local AceGUI = LibStub("AceGUI-3.0");
local LSM = LibStub("LibSharedMedia-3.0");
local LDB = LibStub("LibDataBroker-1.1");
local LDBIcon = LibStub("LibDBIcon-1.0");
local isWindowShown = false;


-- functions
function SEL:OnInitialize()
  -- setup database
  self.db = LibStub("AceDB-3.0"):New("SaucyDBSEL", SEL.defaults, true);

  -- setup the frame
  SEL:SetupFrame();

  -- register interface options
  LibStub("AceConfigRegistry-3.0"):RegisterOptionsTable(SEL.addonName, SEL.options, nil);
  LibStub("AceConfigDialog-3.0"):AddToBlizOptions(SEL.addonName, SEL.addonName);

  -- setup minimap
  local addonLDB = LDB:NewDataObject(SEL.addonName, {
    type = "data source",
    text = SEL.addonName,
    icon = "Interface\\Icons\\inv_drink_13", -- NOTE: Custom way to do it: "Interface\\AddOns\\<ADDON>\\<TEXTURE>"
    OnClick = function(self, button)
      if button == "LeftButton" then
        SEL:ToggleWindow();
      elseif button == "RightButton" then
        SEL:OpenOptions();
      end
    end,
    OnTooltipShow = function(tooltip)
      tooltip:AddDoubleLine(string.format(SEL.Localization["format color"], SEL.db.global.theme.minimap.hex, SEL.addonTitle), string.format(SEL.Localization["format color"], SEL.db.global.theme.minimap.hex, SEL.version));
      tooltip:AddLine(" ");
      tooltip:AddLine(string.format(SEL.Localization["format color value"], SEL.Localization["left click"], SEL.db.global.theme.minimap.hex, SEL.Localization["settings general minimap tooltip left click"]));
      tooltip:AddLine(string.format(SEL.Localization["format color value"], SEL.Localization["right click"], SEL.db.global.theme.minimap.hex, SEL.Localization["settings general minimap tooltip right click"]));
      tooltip:AddLine(string.format(SEL.Localization["format color value"], SEL.Localization["drag"], SEL.db.global.theme.minimap.hex, SEL.Localization["settings general minimap tooltip drag"]));
    end
  });
  LDBIcon:Register(SEL.addonName, addonLDB, SEL.db.profile.minimap);

  -- register slash commands
  SEL:RegisterChatCommand("SEL", "HandleSlashCommands");

  -- show/hide frame on load
  if self.db.profile.general.loadedStartShown then
    SEL.frame:Show();
    isWindowShown = true;
  else
    SEL.frame:Hide();
    isWindowShown = false;
  end

  -- show/hide welcome message on load
  if self.db.profile.general.loadedWelcomeMessage then
    SEL:Print(string.format("%s %s",
      string.format(SEL.Localization["chat welcome loaded"], SEL.version),
      string.format(SEL.Localization["chat welcome help"], string.format(SEL.Localization["format color"], SEL.db.global.theme.commands.hex, SEL.Localization["commands sel help"]))
    ));
  end

  -- register addon for LibSharedMedia
	LSM.RegisterCallback(self, "LibSharedMedia_Registered", function(event, type, key)
    if type == "font" and key == SEL.db.global.font.family then
      SEL:UpdateFrame();
    end
	end)
	LSM.RegisterCallback(self, "LibSharedMedia_SetGlobal", function(event, type)
    if type == "font" then
      SEL:UpdateFrame();
    end
	end)
end

function SEL:HandleSlashCommands(input)
  -- Process the slash command ('input' contains whatever follows the slash command)
  input = string.trim(input, " ");

  -- /SEL
  if input == "" or not input then
    SEL:ToggleWindow();

    return;
  end

  -- /SEL about
  if input == "about" then
    SEL:OpenOptions();

    return;
  end

  -- /SEL config
  if input == "config" then
    SEL:OpenOptions();

    return;
  end

  -- /SEL help, /SEL ?
  if input == "help" or input == "?" then
    SEL:Print(SEL.Localization["chat separator"]);
    SEL:Print(SEL.Localization["version"] ..": "..SEL.version);
    SEL:Print(SEL.Localization["author"] ..": "..SEL.author);
    SEL:Print(string.format(SEL.Localization["format color"], SEL.db.global.theme.commands.hex, SEL.Localization["commands sel"]).." - "..SEL.Localization["chat toggle"]);
    SEL:Print(string.format(SEL.Localization["format color"], SEL.db.global.theme.commands.hex, SEL.Localization["commands sel about"]).." - "..SEL.Localization["chat about"]);
    SEL:Print(string.format(SEL.Localization["format color"], SEL.db.global.theme.commands.hex, SEL.Localization["commands sel config"]).." - "..SEL.Localization["chat config"]);
    SEL:Print(string.format(SEL.Localization["format color"], SEL.db.global.theme.commands.hex, SEL.Localization["commands sel help"]).." - "..SEL.Localization["chat help"]);
    SEL:Print(string.format(SEL.Localization["format color"], SEL.db.global.theme.commands.hex, SEL.Localization["commands sel minimap"]).." - "..SEL.Localization["chat minimap"]);
    SEL:Print(string.format(SEL.Localization["format color"], SEL.db.global.theme.commands.hex, SEL.Localization["commands sel reset"]).." - "..string.format(SEL.Localization["chat reset"], SEL.db.global.theme.warning.hex));

    return;
  end

  -- /SEL minimap
  if input == "minimap" then
    SEL:Minimap();

    return;
  end

  -- /SEL reset
  if input == "reset" then
    SEL:Reset();

    return;
  end

  if input == "debug" then
    SEL.db.global.debug.debugMode = not SEL.db.global.debug.debugMode;

    SEL:Print(string.format(
      SEL.Localization["chat debug"],
      SEL.db.global.debug.debugMode and SEL.db.global.theme.enabled.hex or SEL.db.global.theme.disabled.hex,
      SEL.db.global.debug.debugMode and SEL.Localization["on"] or SEL.Localization["off"]
    ));

    return;
  end

  if input == "test" then
    SEL:Test();

    return;
  end

  SEL:Print(string.format(SEL.Localization["chat command error"], string.format(SEL.Localization["format color"], SEL.db.global.theme.commands.hex, SEL.Localization["commands sel help"])));
end

function SEL:OpenOptions()
  -- NOTE: Two calls are needed because it opens interface options but not on the addon's interface options.
  InterfaceOptionsFrame_OpenToCategory(SEL.addonName);
  InterfaceOptionsFrame_OpenToCategory(SEL.addonName);
end

function SEL:IsShown()
  return isWindowShown;
end

function SEL:ToggleWindow()
  if not isWindowShown then
    if SEL.db.profile.general.sounds then
      PlaySound(882);
    end

    SEL.frame:Show();
    isWindowShown = true;
  else
    SEL.frame:Hide();
    isWindowShown = false;
  end
end

function SEL:CreateButtons(data)
  local button = AceGUI:Create("Button");

  button:SetText(data.name);
  button:SetWidth(125);
  -- button:SetRelativeWidth(0.25);
  -- button:SetFullWidth(true);
  -- widget:SetRelativeWidth(width)

  button:SetCallback("OnClick", function(widget)
    SEL:Debug(data.token);
    DoEmote(data.token);
  end)

  return button;
end


local function DrawGroup1(container)
  local scroll = AceGUI:Create("ScrollFrame")
  scroll:SetLayout("Flow")
  container:AddChild(scroll)

  for index, data in ipairs(SEL.data) do
    local label = SEL:CreateButtons(data);
    scroll:AddChild(label);
  end
end

local function DrawGroup2(container)
  local scroll = AceGUI:Create("ScrollFrame")
  scroll:SetLayout("Flow")
  container:AddChild(scroll)

  for index, data in ipairs(SEL.data) do
    if data.animation then
      local label = SEL:CreateButtons(data);
      scroll:AddChild(label);
    end
  end
end

local function DrawGroup3(container)
  local scroll = AceGUI:Create("ScrollFrame")
  scroll:SetLayout("Flow")
  container:AddChild(scroll)

  for index, data in ipairs(SEL.data) do
    if data.voice then
      local label = SEL:CreateButtons(data);
      scroll:AddChild(label);
    end
  end
end

local function DrawGroup4(container)
  local scroll = AceGUI:Create("ScrollFrame")
  scroll:SetLayout("Flow")
  container:AddChild(scroll)

  for index, data in ipairs(SEL.data) do
    if data.custom then
      local label = SEL:CreateButtons(data);
      scroll:AddChild(label);
    end
  end
end

function SEL:SelectGroup(container, event, group)
  SelectGroup(container, event, group);
end


-- Callback function for OnGroupSelected
local function SelectGroup(container, event, group)
  container:ReleaseChildren()

  if group == "tab1" then
    DrawGroup1(container)
  elseif group == "tab2" then
    DrawGroup2(container)
  elseif group == "tab3" then
    DrawGroup3(container)
  elseif group == "tab4" then
    DrawGroup4(container)
  end

  -- SEL.container = container;
  -- SEL.event = event;
  -- SEL.group = group;
end

function SEL:SetupFrame()



  SEL.frame = SEL.frame or AceGUI:Create("Frame");
  -- SEL.frame = AceGUI:Create("Frame");
  SEL.frame:SetTitle(SEL.addonTitle);
  SEL.frame:SetStatusText(string.format(SEL.Localization["frames general status"], string.format(SEL.Localization["format color"], SEL.db.global.theme.commands.hex, SEL.Localization["commands sel help"])));
  -- SEL.frame:SetWidth(SEL.db.global.frame.width);
  -- SEL.frame:SetHeight(SEL.db.global.frame.height);
  SEL.frame:SetLayout("Fill");
	-- SEL.frame:ClearAllPoints();
	-- SEL.frame:SetPoint(unpack(SEL.db.global.frame.position));

  local tab =  AceGUI:Create("TabGroup")
  tab:SetLayout("Fill")
  tab:SetTabs({
    {text = SEL.Localization["tab 1"], value="tab1"},
    {text = SEL.Localization["tab 2"], value="tab2"},
    {text = SEL.Localization["tab 3"], value="tab3"},
    {text = SEL.Localization["tab 4"], value="tab4"}
  })
  tab:SetCallback("OnGroupSelected", SelectGroup); -- Register callback
  tab:SelectTab("tab1"); -- Set initial Tab (this will fire the OnGroupSelected callback)

  SEL.frame:AddChild(tab)

  SEL.frame:SetCallback("OnClose", function(widget)
    isWindowShown = false;
    SEL:SavePosition();
  end);
end

-- function SEL:UpdateFrame()
--   if SEL.scrollFrame.children then
--     SEL.scrollFrame:ReleaseChildren();
--   end

--   -- SEL.scrollFrame:AddChild(SEL:CreateLabelHeader());

--   for index, data in ipairs(SEL.data) do
--     local label = SEL:CreateButtons(data);
--     SEL.scrollFrame:AddChild(label);
--   end
-- end





function SEL:Minimap()
  SEL.db.profile.minimap.hide = not SEL.db.profile.minimap.hide;

  if SEL.db.profile.minimap.hide then
    LDBIcon:Hide(SEL.addonName);
  else
    LDBIcon:Show(SEL.addonName);
  end
end

function SEL:Popup()
  StaticPopupDialogs[SEL.addonName.."WEBSITE"] = {
    text = SEL.Localization["popup"],
    button1 = SEL.Localization["done"],
    OnShow = function(self, data)
      self.editBox:SetText(SEL.about.issues)
      self.editBox:SetWidth(260)
    end,
    hasEditBox = true,
    exclusive = true,
    whileDead = true,
    hideOnEscape = true,
    preferredIndex = 3
  }

  StaticPopup_Show(SEL.addonName.."WEBSITE")
end

function SEL:SavePosition()
  SEL:Debug(SEL.Localization["saving position"]);

  local point, _, relativePoint, xOffset, yOffset = SEL.frame:GetPoint();
	local position = {point, "UIParent", relativePoint, xOffset, yOffset};

  SEL.db.global.frame.position = position;

	SEL.frame:ClearAllPoints();
	SEL.frame:SetPoint(unpack(SEL.db.global.frame.position));
end


function SEL:Reset()
  for k,v in pairs(SEL.defaults.global) do
    SEL.db.global[k] = v;
  end

  for k,v in pairs(SEL.defaults.profile) do
    SEL.db.profile[k] = v;
  end

  SEL.db.profile.minimap.hide = SEL.defaults.profile.minimap.hide;

  if SEL.db.profile.minimap.hide then
    LDBIcon:Hide(SEL.addonName);
  else
    LDBIcon:Show(SEL.addonName);
  end

  if SEL.db.profile.general.loadedStartShown then
    SEL.frame:Show();
    isWindowShown = true;
  else
    SEL.frame:Hide();
    isWindowShown = false;
  end

  SEL:UpdateFrame();

  SEL:Print(SEL.Localization["chat reset settings"]);
end

-- TODO: Is there a better way of resetting colors? This doesn't work after first reset (changes the colors in the UI).
function SEL:ResetColors()
  for k,v in pairs(SEL.defaults.profile.general.colors) do
    SEL.db.profile.general.colors[k] = v;
  end

  SEL:Debug(SEL.Localization["chat reset colors"])
end

-- debugging
function SEL:Debug(key, value)
  if SEL.db.global.debug.debugMode and SEL.db.global.debug.debugMessages then
    if value ~= nil then
      SEL:Print(tostring(key)..": "..tostring(value));
    else
      SEL:Print(tostring(key));
    end
  end
end

function SEL:DebugSettings(info, value)
  SEL:Debug(info[#info], value);
end

function SEL:DebugFrame()
  if not SEL.db.global.debug.debugMode then return end

  SEL.debugFrame = AceGUI:Create("Frame");
  SEL.debugFrame:SetTitle(SEL.addonTitle..SEL.Localization["frames debug title"]);
  SEL.debugFrame:SetStatusText(SEL.Localization["frames debug status"]);
  SEL.debugFrame:SetFullWidth(true);
  SEL.debugFrame:SetFullHeight(true);
  SEL.debugFrame:SetLayout("Fill");

  local scroll = scroll or AceGUI:Create("ScrollFrame");
  scroll:SetLayout("Flow");
  SEL.debugFrame:AddChild(scroll);

  for i=1, 60 do
    local label1 = SEL:DebugLabel("playerLevel: 30 - targetLevel: "..i, SEL:CalcLevels(30, i));
    scroll:AddChild(label1);
  end

  local heading1 = AceGUI:Create("Heading");
  scroll:AddChild(heading1);

  for i=1, 70 do
    local label2 = SEL:DebugLabel("playerLevel: 60 - targetLevel: "..i, SEL:CalcLevels(60, i));
    scroll:AddChild(label2);
  end
end

function SEL:DebugLabel(text, color)
  local label = AceGUI:Create("Label");

  label:SetText(text);
  label:SetFullWidth(true);
  label:SetFontObject(GameFontHighlight);
  label:SetColor(unpack(color));

  return label;
end

function SEL:Test()
  SEL:TestFrame2();
  -- SEL:SetupFrame();
end

function SEL:TestFrame1()
  local topContainer = AceGUI:Create("Frame")
  topContainer:SetTitle("frame test");
  topContainer:SetStatusText("status text test");
  topContainer:SetFullWidth(true)
  topContainer:SetFullHeight(true) -- probably?
  topContainer:SetLayout("Fill");

  local scrollcontainer = AceGUI:Create("SimpleGroup") -- "InlineGroup" is also good
  scrollcontainer:SetFullWidth(true)
  scrollcontainer:SetFullHeight(true) -- probably?
  scrollcontainer:SetLayout("Fill") -- important!

  topContainer:AddChild(scrollcontainer)

  scroll = AceGUI:Create("ScrollFrame")
  scroll:SetLayout("Flow") -- probably?
  scrollcontainer:AddChild(scroll)

  if scroll.children then
    scroll:ReleaseChildren();
  end

  for index, data in ipairs(SEL.data) do
    local label = SEL:CreateButtons(data);
    scroll:AddChild(label);
  end
end

function SEL:TestFrame2()
  local function DrawGroup1(container)
    local scroll = AceGUI:Create("ScrollFrame")
    scroll:SetLayout("Flow")
    container:AddChild(scroll)

    for index, data in ipairs(SEL.data) do
      local label = SEL:CreateButtons(data);
      scroll:AddChild(label);
    end
  end

  -- function that draws the widgets for the second tab
  local function DrawGroup2(container)
    local scroll = AceGUI:Create("ScrollFrame")
    scroll:SetLayout("Flow")
    container:AddChild(scroll)

    for index, data in ipairs(SEL.data) do
      if data.animation then
        local label = SEL:CreateButtons(data);
        scroll:AddChild(label);
      end
    end
  end

  local function DrawGroup3(container)
    local scroll = AceGUI:Create("ScrollFrame")
    scroll:SetLayout("Flow")
    container:AddChild(scroll)

    for index, data in ipairs(SEL.data) do
      if data.voice then
        local label = SEL:CreateButtons(data);
        scroll:AddChild(label);
      end
    end
  end

  local function DrawGroup4(container)
    local scroll = AceGUI:Create("ScrollFrame")
    scroll:SetLayout("Flow")
    container:AddChild(scroll)

    for index, data in ipairs(SEL.data) do
      if data.custom then
        local label = SEL:CreateButtons(data);
        scroll:AddChild(label);
      end
    end
  end

  -- Callback function for OnGroupSelected
  local function SelectGroup(container, event, group)
    container:ReleaseChildren()

    if group == "tab1" then
        DrawGroup1(container)
    elseif group == "tab2" then
        DrawGroup2(container)
    elseif group == "tab3" then
        DrawGroup3(container)
    elseif group == "tab4" then
        DrawGroup4(container)
    end
  end

  -- Create the frame container
  local frame = AceGUI:Create("Frame")
  frame:SetTitle("Example Frame")
  frame:SetStatusText("AceGUI-3.0 Example Container Frame")
  frame:SetCallback("OnClose", function(widget) AceGUI:Release(widget) end)
  -- Fill Layout - the TabGroup widget will fill the whole frame
  frame:SetLayout("Fill")

  -- Create the TabGroup
  local tab =  AceGUI:Create("TabGroup")
  -- tab:SetLayout("Flow")
  tab:SetLayout("Fill")
  -- Setup which tabs to show
  tab:SetTabs({
    {text="All", value="tab1"},
    {text="Animations", value="tab2"},
    {text="Voice", value="tab3"},
    {text="Custom", value="tab4"}
  })
  -- Register callback
  tab:SetCallback("OnGroupSelected", SelectGroup)
  -- Set initial Tab (this will fire the OnGroupSelected callback)
  tab:SelectTab("tab1")

  -- add to the frame container
  frame:AddChild(tab)
end

