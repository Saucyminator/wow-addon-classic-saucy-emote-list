SEL.Localization = LibStub("AceLocale-3.0"):GetLocale("SEL", true);


-- about
SEL.about = {
  website = "",
  issues = "https://gitlab.com/Saucyminator/wow-addon-classic-saucy-emote-list/issues"
}


-- known issues
SEL.knownIssues = [[TODO]];


-- changelog
SEL.changelog = [[TODO]];


-- config
SEL.defaults = {
  global = {
    debug = {
      debugMode = false,
      debugMessages = false,
      debugLevel = 30,
      debugColors = {
        gray = { red = 0.5, green = 0.5, blue = 0.5},
        green = { red = 0, green = 0.5, blue = 0}
      }
    },
    theme = {
      commands = { red = 0, green = 0.8, blue = 1, hex = "00ccff" },
      warning = { red = 1, green = 0, blue = 0, hex = "ff0000" },
      minimap = { red = 1, green = 1, blue = 1, hex = "ffffff" },
      enabled = { red = 0, green = 1, blue = 0, hex = "00ff00" },
      disabled = { red = 1, green = 0, blue = 0, hex = "ff0000" }
    },
    frame = {
      scale = 1,
      width = 500,
      height = 530,
	    -- locked = false,
	    position = {"LEFT", "UIParent", "LEFT", 50, 0}
    },
    font = {
      family = "Friz Quadrata TT",
      size = 12
    }
  },
  profile = {
    general = {
      loadedWelcomeMessage = true, -- display welcome message
      loadedStartShown = false, -- display at start
      colorText = true, -- display colors for each row
      colorsBasedOnLastBossLevel = true, -- calculate colors based on last boss level
      colors = {
        gray = { red = 0.5, green = 0.5, blue = 0.5, hex = "808080"},
        green = { red = 0, green = 0.5, blue = 0, hex = "008000"},
        yellow = { red = 1, green = 1, blue = 0, hex = "ffff00"},
        orange = { red = 1, green = 0.65, blue = 0, hex = "ffa500"},
        red = { red = 0.86, green = 0.08, blue = 0.24, hex = "dc143c"},
        boss = { red = 1, green = 0, blue = 0, hex = "ff0000"}
      },
      textAbbreviation = true,
      textLastBossLevel = true, -- display last boss' level
      textRecommendedLevels = true, -- display recommended levels
      displayRaids = false, -- display raid dungeons
      sounds = true
    },
    minimap = {
      hide = false
    }
  }
}

-- options
SEL.options = {
	name = SEL.addonName,
  handler = SEL,
  type = "group",
  childGroups = "tab",
	args = {
    intro = {
      order = 0,
      name = string.format(SEL.Localization["settings description"], SEL.version, SEL.about.issues),
      type = "description"
    },

		general_tab = {
			order = 1,
			name = SEL.Localization["settings tab general"],
			type = "group",
			args = {
        headerLoaded = {
          order = 1,
          name = SEL.Localization["settings general loaded header"],
					type = "header",
        },
        loadedWelcomeMessage = {
          order = 2,
          name = SEL.Localization["settings general loaded welcome message"],
          desc = SEL.Localization["settings general loaded welcome message desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SEL.db.profile.general.loadedWelcomeMessage
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.profile.general.loadedWelcomeMessage = value
          end
        },
        loadedStartShown = {
          order = 3,
          name = SEL.Localization["settings general loaded start shown"],
          desc = SEL.Localization["settings general loaded start shown desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SEL.db.profile.general.loadedStartShown
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.profile.general.loadedStartShown = value
          end
        },
        headerColors = {
          order = 4,
          name = SEL.Localization["settings general colors header"],
					type = "header",
        },
        colorText = {
          order = 5,
          name = SEL.Localization["settings general colors row"],
          desc = SEL.Localization["settings general colors row desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SEL.db.profile.general.colorText;
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.profile.general.colorText = value;

            SEL:UpdateFrame();
          end
        },
        colorsBasedOnLastBossLevel = {
          order = 6,
          name = SEL.Localization["settings general colors based on last boss level"],
          desc = SEL.Localization["settings general colors based on last boss level desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SEL.db.profile.general.colorsBasedOnLastBossLevel;
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.profile.general.colorsBasedOnLastBossLevel = value;

            SEL:UpdateFrame();
          end
        },
        colors = {
          order = 7,
          name = SEL.Localization["settings general colors level colors"],
          desc = SEL.Localization["settings general colors level colors desc"],
          type = "group",
          inline = true,
          get = function (info)
            local color = {
              SEL.db.profile.general.colors[info[3]].red,
              SEL.db.profile.general.colors[info[3]].green,
              SEL.db.profile.general.colors[info[3]].blue,
              1
            }

            return unpack(color);
          end,
          set = function (info, red, green, blue)
            SEL:DebugSettings(info, {red, green, blue});

            SEL.db.profile.general.colors[info[3]].red = red;
            SEL.db.profile.general.colors[info[3]].green = green;
            SEL.db.profile.general.colors[info[3]].blue = blue;

            SEL:UpdateFrame();
          end,
          args = {
            gray = {
              order = 1,
              name = SEL.Localization["settings general colors gray"],
              desc = SEL.Localization["settings general colors gray desc"],
              type = "color",
              width = 1.5,
              hasAlpha = false
            },
            green = {
              order = 3,
              name = SEL.Localization["settings general colors green"],
              desc = SEL.Localization["settings general colors green desc"],
              type = "color",
              width = 1.5,
              hasAlpha = false
            },
            yellow = {
              order = 5,
              name = SEL.Localization["settings general colors yellow"],
              desc = SEL.Localization["settings general colors yellow desc"],
              type = "color",
              width = 1.5,
              hasAlpha = false
            },
            orange = {
              order = 2,
              name = SEL.Localization["settings general colors orange"],
              desc = SEL.Localization["settings general colors orange desc"],
              type = "color",
              width = 1.5,
              hasAlpha = false
            },
            red = {
              order = 4,
              name = SEL.Localization["settings general colors red"],
              desc = SEL.Localization["settings general colors red desc"],
              type = "color",
              width = 1.5,
              hasAlpha = false
            },
            boss = {
              order = 6,
              name = SEL.Localization["settings general colors boss"],
              desc = SEL.Localization["settings general colors boss desc"],
              type = "color",
              width = 1.5,
              hasAlpha = false
            }
          }
        },
        resetColors = {
          order = 8,
          name = SEL.Localization["settings general colors reset"],
          desc = SEL.Localization["settings general colors reset desc"],
          type = "execute",
          func = function(info, value)
            SEL:DebugSettings(info, SEL.Localization["settings general colors reset"]);

            SEL:ResetColors();

            SEL:UpdateFrame();
          end
        },
        headerText = {
          order = 9,
          name = SEL.Localization["settings general text header"],
          type = "header",
        },
        textRecommendedLevels = {
          order = 10,
          name = SEL.Localization["settings general text recommended levels"],
          desc = SEL.Localization["settings general text recommended levels desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SEL.db.profile.general.textRecommendedLevels
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.profile.general.textRecommendedLevels = value;

            SEL:UpdateFrame();
          end
        },
        textAbbreviation = {
          order = 11,
          name = SEL.Localization["settings general text abbreviation"],
          desc = SEL.Localization["settings general text abbreviation desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SEL.db.profile.general.textAbbreviation
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.profile.general.textAbbreviation = value;

            SEL:UpdateFrame();
          end
        },
        textLastBossLevel = {
          order = 12,
          name = SEL.Localization["settings general text last boss level"],
          desc = SEL.Localization["settings general text last boss level desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SEL.db.profile.general.textLastBossLevel
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.profile.general.textLastBossLevel = value;

            SEL:UpdateFrame();
          end
        },
        headerDisplay = {
          order = 13,
          name = SEL.Localization["settings general display rows header"],
          type = "header",
        },
        displayRaids = {
          order = 14,
          name = SEL.Localization["settings general display raids"],
          desc = SEL.Localization["settings general display raids desc"],
          type = "toggle",
          width = "full",
          get = function ()
            return SEL.db.profile.general.displayRaids
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.profile.general.displayRaids = value;

            SEL:UpdateFrame();
          end
        },
        headerSounds = {
          order = 21,
          name = SEL.Localization["settings general sounds header"],
          type = "header",
        },
        sounds = {
          order = 22,
          name = SEL.Localization["settings general sounds"],
          desc = SEL.Localization["settings general sounds desc"],
          type = "toggle",
          width = 1.5,
          get = function ()
            return SEL.db.profile.general.sounds
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.profile.general.sounds = value;
          end
        },
        headerFont = {
          order = 23,
          name = SEL.Localization["settings general font header"],
          type = "header",
        },
        fontFamily = {
          order = 24,
          name = SEL.Localization["settings general font size"],
          desc = SEL.Localization["settings general font size desc"],
          type = "select",
          width = 1.5,
          dialogControl = "LSM30_Font",
          values = AceGUIWidgetLSMlists.font,
          get = function ()
            return SEL.db.global.font.family;
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.global.font.family = value;

            SEL:UpdateFrame();
          end
        },
        fontSize = {
          order = 25,
          name = SEL.Localization["settings general font size"],
          desc = SEL.Localization["settings general font size desc"],
          type = "range",
          width = 1.5,
          min = 7,
          max = 24,
          step = 1,
          get = function ()
            return SEL.db.global.font.size;
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.global.font.size = value;

            SEL:UpdateFrame();
          end
        },
        headerMinimap = {
          order = 28,
          name = SEL.Localization["settings general minimap header"],
					type = "header"
        },
				toggleShown = {
					order = 29,
          name = SEL.Localization["settings general minimap"],
          desc = SEL.Localization["settings general minimap desc"],
					type = "toggle",
					width = "full",
          get = function ()
            return not SEL.db.profile.minimap.hide;
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL:Minimap();
          end
        }

      }
    },

    about_tab = {
      order = 2,
      name = SEL.Localization["settings tab about"],
      type = "group",
      args = {
				labelAbout = {
					order = 2,
          name = string.format(SEL.Localization["settings about"], SEL.author),
          desc = SEL.Localization["settings about desc"],
					type = "description",
          width = "full",
          fontSize = "medium"
        },
        headerKnownIssues = {
          order = 10,
          name = SEL.Localization["settings about known issues header"],
					type = "header"
        },
				labelKnownIssues = {
					order = 11,
          name = string.format(SEL.Localization["settings about known issues"], SEL.knownIssues),
          desc = SEL.Localization["settings about known issues desc"],
					type = "description",
          width = "full",
          fontSize = "medium"
        },
        headerIssues = {
          order = 20,
          name = SEL.Localization["settings about issues header"],
					type = "header"
        },
				labelIssues = {
					order = 21,
          name = string.format(SEL.Localization["settings about issues"], SEL.about.issues),
          desc = SEL.Localization["settings about issues desc"],
					type = "description",
					width = "full",
          fontSize = "medium"
        },
        popup = {
          order = 22,
          name = SEL.Localization["settings about issues button report"],
          desc = SEL.Localization["settings about issues button report desc"],
          type = "execute",
          width = 1,
          func = function (info, value)
            SEL:Popup()
          end
        }
      }
    },

		debug_tab = {
			order = 99,
			name = SEL.Localization["settings tab debug"],
			type = "group",
			args = {
				debugHeader = {
					order = 1,
					name = SEL.Localization["settings debug header"],
					type = "header",
				},
				debugMode = {
					order = 2,
          name = SEL.Localization["settings debug mode"],
          desc = SEL.Localization["settings debug mode desc"],
					type = "toggle",
					width = "full",
          get = function ()
            return SEL.db.global.debug.debugMode;
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.global.debug.debugMode = value;

            SEL:UpdateFrame();
            -- SEL:SelectGroup(SEL.container, SEL.event, SEL.group);
            -- SelectGroup(SEL.container, SEL.event, SEL.group);
          end
        },
				debugMessages = {
					order = 3,
          name = SEL.Localization["settings debug chat messages"],
          desc = SEL.Localization["settings debug chat messages desc"],
					type = "toggle",
					width = "full",
          get = function ()
            return SEL.db.global.debug.debugMessages;
          end,
          set = function (info, value)
            SEL:DebugSettings(info, value);

            SEL.db.global.debug.debugMessages = value;

            SEL:UpdateFrame();
          end
        },
        debugLevel = {
          order = 3,
          name = SEL.Localization["settings debug test level range"],
          desc = SEL.Localization["settings debug test level range desc"],
          type = "range",
					width = "full",
          min = 1,
          max = 60,
          step = 1,
          get = function ()
            return SEL.db.global.debug.debugLevel;
          end,
          set = function(info, value)
            SEL:DebugSettings(info, value);

            SEL.db.global.debug.debugLevel = value;

            SEL:UpdateFrame();
          end
        },
				debugResetHeader = {
					order = 98,
					name = SEL.Localization["settings debug reset header"],
					type = "header",
				},
        debugResetButton = {
          order = 99,
          name = SEL.Localization["settings debug reset"],
          desc = SEL.Localization["settings debug reset desc"],
          type = "execute",
					width = "full",
          func = function(info, value)
            SEL:DebugSettings(info, value);

            SEL:Reset();
          end
        },
        debugColors = {
          order = 100,
          name = "debug color",
          type = "group",
          inline = true,
          get = function (info)
            local color = {
              SEL.db.global.debug.debugColors[info[3]].red,
              SEL.db.global.debug.debugColors[info[3]].green,
              SEL.db.global.debug.debugColors[info[3]].blue,
            }

            return unpack(color);
          end,
          set = function (info, red, green, blue)
            SEL:DebugSettings(info, {red, green, blue});

            SEL.db.global.debug.debugColors[info[3]].red = red;
            SEL.db.global.debug.debugColors[info[3]].green = green;
            SEL.db.global.debug.debugColors[info[3]].blue = blue;
          end,
          args = {
            gray = {
              order = 1,
              name = "debug gray",
              desc = "",
              type = "color",
              width = 1.5,
              hasAlpha = false
            },
            green = {
              order = 3,
              name = "debug green",
              desc = "",
              type = "color",
              width = 1.5,
              hasAlpha = false
            }
          }
        },
        debugResetColors = {
          order = 101,
          name = SEL.Localization["settings general colors reset"],
          desc = SEL.Localization["settings general colors reset desc"],
          type = "execute",
          func = function(info, value)
            SEL:DebugSettings(info, SEL.Localization["settings general colors reset"]);

            for k,v in pairs(SEL.defaults.global.debug.debugColors) do
              SEL.db.global.debug.debugColors[k] = v;
              print(k, v)
            end
          end
        },
			}
    },

    changelog_tab = {
      order = 100,
			name = SEL.Localization["settings tab changelog"],
			type = "group",
			args = {
				labelChangelog = {
					order = 1,
          name = string.format(SEL.Localization["settings changelog"], SEL.changelog),
          desc = SEL.Localization["settings changelog desc"],
					type = "description",
          width = "full",
          fontSize = "medium"
        },
      }
    }
	}
}

-- data
SEL.data = {
  { -- verified
    order = 1,
    name = "/agree",
    token = "AGREE",
    animation = false,
    voice = false,
    noTarget = "You agree.",
    target = "You agree with <target>.",
    custom = true,
    customOrder = 0
  },
  { -- verified
    order = 2,
    name = "/amaze",
    token = "AMAZE",
    animation = false,
    voice = false,
    noTarget = "You are amazed!",
    target = "You are amazed by <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 3,
    name = "/angry",
    token = "ANGRY",
    animation = true,
    voice = false,
    noTarget = "You raise your fist in anger.",
    target = "You raise your fist in anger at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 4,
    name = "/apologize",
    token = "APOLOGIZE",
    animation = false,
    voice = false,
    noTarget = "You apologize to everyone. Sorry!",
    target = "You apologize to <target>. Sorry!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 5,
    name = "/applaud", -- or /applause
    token = "APPLAUD",
    animation = true,
    voice = true,
    noTarget = "You applaud. Bravo!",
    target = "You applaud at <target>. Bravo!",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 6,
    name = "/attacktarget",
    token = "ATTACKTARGET",
    animation = true,
    voice = true,
    noTarget = "You tell everyone to attack something.",
    target = "You tell everyone to attack <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 7,
    name = "/bark",
    token = "BARK",
    animation = false,
    voice = false,
    noTarget = "You bark. Woof woof!",
    target = "You bark at <target>",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 8,
    name = "/bashful",
    token = "BASHFUL",
    animation = true,
    voice = false,
    noTarget = "You are bashful.",
    target = "You are so bashful...too bashful to get <target>'s attention.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 9,
    name = "/beckon",
    token = "BECKON",
    animation = false,
    voice = false,
    noTarget = "You beckon everyone over to you.",
    target = "You beckon <target> over.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 10,
    name = "/beg",
    token = "BEG",
    animation = true,
    voice = true,
    noTarget = "You beg everyone around you. How pathetic.",
    target = "You beg <target>. How pathetic.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 11,
    name = "/belch",
    token = "BELCH",
    animation = false,
    voice = false,
    noTarget = "You let out a loud belch.",
    target = "You burp rudely in <target>'s face.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 12,
    name = "/bite",
    token = "BITE",
    animation = false,
    voice = false,
    noTarget = "You look around for someone to bite.",
    target = "You bite <target>. Ouch!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 13,
    name = "/bleed", -- or /blood
    token = "BLEED",
    animation = false,
    voice = false,
    noTarget = "Blood oozes from your wounds.",
    target = "",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 14,
    name = "/blink",
    token = "BLINK",
    animation = false,
    voice = false,
    noTarget = "You blink your eyes.",
    target = "You blink at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 15,
    name = "/blow",
    token = "BLOW",
    animation = true,
    voice = true,
    noTarget = "You blow a kiss into the wind.",
    target = "You blow a kiss to <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 16,
    name = "/blush",
    token = "BLUSH",
    animation = true,
    voice = false,
    noTarget = "You blush.",
    target = "You blush at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 17,
    name = "/boggle",
    token = "BOGGLE",
    animation = true,
    voice = false,
    noTarget = "You boggle at the situation.",
    target = "You boggle at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 18,
    name = "/bonk",
    token = "BONK",
    animation = false,
    voice = false,
    noTarget = "You bonk yourself on the noggin. Doh!",
    target = "You bonk <target> on the noggin. Doh!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 19,
    name = "/bored",
    token = "BORED",
    animation = false,
    voice = true,
    noTarget = "You are overcome with boredom. Oh the drudgery!",
    target = "You are terribly bored with <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 20,
    name = "/bounce",
    token = "BOUNCE",
    animation = false,
    voice = false,
    noTarget = "You bounce up and down.",
    target = "You bounce up and down in front of <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 21,
    name = "/bow",
    token = "BOW",
    animation = true,
    voice = false,
    noTarget = "You bow down graciously.",
    target = "You bow before <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 22,
    name = "/bravo",
    token = "BRAVO",
    animation = true,
    voice = true,
    noTarget = "You applaud. Bravo!",
    target = "You applaud at <target>. Bravo!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 23,
    name = "/brb",
    token = "BRB",
    animation = false,
    voice = false,
    noTarget = "You let everyone know you'll be right back",
    target = "You let <target> know you'll be right back.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 24,
    name = "/burp",
    token = "BURP",
    animation = false,
    voice = false,
    noTarget = "You let out a loud belch.",
    target = "You burp rudely in <target>'s face.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 25,
    name = "/bye",
    token = "BYE",
    animation = true,
    voice = true,
    noTarget = "You wave goodbye to everyone. Farewell!",
    target = "You wave goodbye to <target>. Farewell!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 26,
    name = "/cackle",
    token = "CACKLE",
    animation = true,
    voice = true,
    noTarget = "You cackle maniacally at the situation.",
    target = "You cackle maniacally at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 27,
    name = "/calm",
    token = "CALM",
    animation = false,
    voice = false,
    noTarget = "You remain calm.",
    target = "You try to calm <target> down.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 27,
    name = "/cat", -- or /catty
    token = "CAT",
    animation = false,
    voice = false,
    noTarget = "You scratch yourself. Ah, much better!",
    target = "You scratch <target>. How catty!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 28,
    name = "/charge",
    token = "CHARGE",
    animation = true,
    voice = true,
    noTarget = "You start to charge.",
    target = "",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 29,
    name = "/cheer",
    token = "CHEER",
    animation = true,
    voice = true,
    noTarget = "You cheer!",
    target = "You cheer at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 30,
    name = "/chew",
    token = "CHEW",
    animation = true,
    voice = false,
    noTarget = "You begin to eat.",
    target = "You begin to eat in front of <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 31,
    name = "/chicken",
    token = "CHICKEN",
    animation = true,
    voice = true,
    noTarget = "With arms flapping, you strut around. Cluck, Cluck, Chicken!",
    target = "With arms flapping, you strut around <target>. Cluck, Cluck, Chicken!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 32,
    name = "/chuckle",
    token = "CHUCKLE",
    animation = true,
    voice = true,
    noTarget = "You let out a hearty chuckle.",
    target = "You chuckle at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 33,
    name = "/clap",
    token = "CLAP",
    animation = true,
    voice = true,
    noTarget = "You clap excitedly.",
    target = "You clap excitedly for <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 34,
    name = "/cold",
    token = "COLD",
    animation = false,
    voice = false,
    noTarget = "You let everyone know that you are cold.",
    target = "You let <target> know that you are cold.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 35,
    name = "/comfort",
    token = "COMFORT",
    animation = false,
    voice = false,
    noTarget = "You need to be comforted.",
    target = "You comfort <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 36,
    name = "/commend",
    token = "COMMEND",
    animation = true,
    voice = true,
    noTarget = "You commend everyone on a job well done.",
    target = "You commend <target> on a job well done.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 37,
    name = "/confused",
    token = "CONFUSED",
    animation = true,
    voice = false,
    noTarget = "You are hopelessly confused.",
    target = "You look at <target> with a confused look.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 38,
    name = "/congrats", -- or /cong or /congratulate
    token = "CONGRATS",
    animation = true,
    voice = true,
    noTarget = "You congratulate everyone around you.",
    target = "You congratulate <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 39,
    name = "/cough",
    token = "COUGH",
    animation = false,
    voice = false,
    noTarget = "You let out a hacking cough.",
    target = "You cough at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 40,
    name = "/cower",
    token = "COWER",
    animation = true,
    voice = false,
    noTarget = "You cower in fear.",
    target = "You cower in fear at the sight of <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 41,
    name = "/crack",
    token = "CRACK",
    animation = false,
    voice = false,
    noTarget = "You crack your knuckles.",
    target = "You crack your knuckles while staring at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 42,
    name = "/cringe",
    token = "CRINGE",
    animation = false,
    voice = false,
    noTarget = "You cringe in fear.",
    target = "You cringe away from <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 43,
    name = "/cry",
    token = "CRY",
    animation = true,
    voice = true,
    noTarget = "You cry.",
    target = "You cry on <target>'s shoulder.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 44,
    name = "/cuddle",
    token = "CUDDLE",
    animation = false,
    voice = false,
    noTarget = "You need to be cuddled.",
    target = "You cuddle up against <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 45,
    name = "/curious",
    token = "CURIOUS",
    animation = true,
    voice = false,
    noTarget = "You express your curiosity to those around you.",
    target = "You are curious what <target> is up to.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 46,
    name = "/curtsey",
    token = "CURTSEY",
    animation = true,
    voice = false,
    noTarget = "You curtsey.",
    target = "You curtsey before <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 47,
    name = "/dance",
    token = "DANCE",
    animation = true,
    voice = false,
    noTarget = "You burst into dance.",
    target = "You dance with <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 48,
    name = "/disappointed", -- or /disappointment
    token = "DISAPPOINTED",
    animation = false,
    voice = false,
    noTarget = "You frown.",
    target = "You frown with disappointment at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 49,
    name = "/doh",
    token = "DOH",
    animation = false,
    voice = false,
    noTarget = "You bonk yourself on the noggin. Doh!",
    target = "You bonk <target> on the noggin. Doh!",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 50,
    name = "/doom",
    token = "DOOM",
    animation = false,
    voice = true,
    noTarget = "You threaten everyone with the wrath of doom.",
    target = "You threaten <target> with the wrath of doom.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 51,
    name = "/drink",
    token = "DRINK",
    animation = true,
    voice = false,
    noTarget = "You raise a drink in the air before chugging it down. Cheers!",
    target = "You raise a drink to <target>. Cheers!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 52,
    name = "/drool",
    token = "DROOL",
    animation = false,
    voice = false,
    noTarget = "A tendril of drool runs down your lip.",
    target = "You look at <target> and begin to drool.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 53,
    name = "/duck",
    token = "DUCK",
    animation = false,
    voice = false,
    noTarget = "You duck for cover.",
    target = "You duck behind <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 54,
    name = "/eat",
    token = "EAT",
    animation = true,
    voice = false,
    noTarget = "You begin to eat.",
    target = "You begin to eat in front of <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 55,
    name = "/excited",
    token = "EXCITED",
    animation = true,
    voice = false,
    noTarget = "You talk excitedly with everyone.",
    target = "You talk excitedly with <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 56,
    name = "/eye",
    token = "EYE",
    animation = false,
    voice = false,
    noTarget = "You cross your eyes.",
    target = "You eye <target> up and down.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 57,
    name = "/farewell",
    token = "FAREWELL",
    animation = true,
    voice = true,
    noTarget = "You wave goodbye to everyone. Farewell!",
    target = "You wave goodbye to <target>. Farewell!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 58,
    name = "/fart",
    token = "FART",
    animation = false,
    voice = false,
    noTarget = "You fart loudly. Whew...what stinks?",
    target = "You brush up against <target> and fart loudly.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 59,
    name = "/fear",
    token = "FEAR",
    animation = false,
    voice = false,
    noTarget = "You cower in fear.",
    target = "You cower in fear at the sight of <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 60,
    name = "/feast",
    token = "FEAST",
    animation = true,
    voice = false,
    noTarget = "You begin to eat.",
    target = "You begin to eat in front of <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 61,
    name = "/fidget",
    token = "FIDGET",
    animation = false,
    voice = false,
    noTarget = "You fidget.",
    target = "You fidget impatiently while waiting for <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 62,
    name = "/flap",
    token = "FLAP",
    animation = true,
    voice = true,
    noTarget = "With arms flapping, you strut around. Cluck, Cluck, Chicken!",
    target = "With arms flapping, you strut around <target>. Cluck, Cluck, Chicken!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 63,
    name = "/flee",
    token = "FLEE",
    animation = true,
    voice = true,
    noTarget = "You yell for everyone to flee!",
    target = "You yell for <target> to flee!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 64,
    name = "/flex",
    token = "FLEX",
    animation = true,
    voice = false,
    noTarget = "You flex your muscles. Oooooh so strong!",
    target = "You flex at <target>. Oooooh so strong!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 65,
    name = "/flirt",
    token = "FLIRT",
    animation = true,
    voice = true,
    noTarget = "You flirt.",
    target = "You flirt with <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 66,
    name = "/flop",
    token = "FLOP",
    animation = false,
    voice = false,
    noTarget = "You flop about helplessly.",
    target = "You flop about helplessly around <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 67,
    name = "/followme",
    token = "FOLLOWME",
    animation = true,
    voice = true,
    noTarget = "You motion for everyone to follow.",
    target = "You motion for <target> to follow.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 68,
    name = "/food",
    token = "FOOD",
    animation = false,
    voice = false,
    noTarget = "You are hungry!",
    target = "You are hungry. Maybe <target> has some food...",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 69,
    name = "/frown",
    token = "FROWN",
    animation = false,
    voice = false,
    noTarget = "You frown.",
    target = "You frown with disappointment at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 70,
    name = "/gasp",
    token = "GASP",
    animation = true,
    voice = false,
    noTarget = "You gasp.",
    target = "You gasp at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 71,
    name = "/gaze",
    token = "GAZE",
    animation = false,
    voice = false,
    noTarget = "You gaze off into the distance.",
    target = "You gaze longingly at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 72,
    name = "/giggle",
    token = "GIGGLE",
    animation = true,
    voice = true,
    noTarget = "You giggle.",
    target = "You giggle at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 73,
    name = "/glad",
    token = "GLAD",
    animation = false,
    voice = false,
    noTarget = "You are filled with happiness!",
    target = "You are very happy with <target>!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 74,
    name = "/glare",
    token = "GLARE",
    animation = false,
    voice = false,
    noTarget = "You glare angrily.",
    target = "You glare angrily at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 75,
    name = "/gloat",
    token = "GLOAT",
    animation = true,
    voice = true,
    noTarget = "You gloat over everyone's misfortune.",
    target = "You gloat over <target>'s misfortune.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 76,
    name = "/golfclap",
    token = "GOLFCLAP",
    animation = true,
    voice = true,
    noTarget = "You clap half heartedly, clearly unimpressed.",
    target = "You clap for <target>, clearly unimpressed.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 77,
    name = "/goodbye",
    token = "GOODBYE",
    animation = true,
    voice = true,
    noTarget = "You wave goodbye to everyone. Farewell!",
    target = "You wave goodbye to <target>. Farewell!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 78,
    name = "/greet", -- or /greetings
    token = "GREET",
    animation = true,
    voice = false,
    noTarget = "You greet everyone warmly.",
    target = "You greet <target> warmly.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 79,
    name = "/grin",
    token = "GRIN",
    animation = false,
    voice = false,
    noTarget = "You grin wickedly.",
    target = "You grin wickedly at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 80,
    name = "/groan",
    token = "GROAN",
    animation = false,
    voice = false,
    noTarget = "You begin to groan.",
    target = "You look at <target> and groan.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 81,
    name = "/grovel",
    token = "GROVEL",
    animation = true,
    voice = false,
    noTarget = "You grovel on the ground, wallowing in subservience.",
    target = "You grovel before <target> like a subservient peon.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 82,
    name = "/growl",
    token = "GROWL",
    animation = true,
    voice = false,
    noTarget = "You growl menacingly.",
    target = "You growl menacingly at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 83,
    name = "/guffaw",
    token = "GUFFAW",
    animation = true,
    voice = true,
    noTarget = "You let out a boisterous guffaw!",
    target = "You take one look at <target> and let out a guffaw!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 84,
    name = "/hail",
    token = "HAIL",
    animation = true,
    voice = false,
    noTarget = "You hail those around you.",
    target = "You hail <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 85,
    name = "/happy",
    token = "HAPPY",
    animation = false,
    voice = false,
    noTarget = "You are filled with happiness!",
    target = "You are very happy with <target>!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 86,
    name = "/healme",
    token = "HEALME",
    animation = true,
    voice = true,
    noTarget = "You call out for healing!",
    target = "",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 87,
    name = "/hello",
    token = "HELLO",
    animation = true,
    voice = true,
    noTarget = "You greet everyone with a hearty hello!",
    target = "You greet <target> with a hearty hello!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 88,
    name = "/helpme",
    token = "HELPME",
    animation = true,
    voice = true,
    noTarget = "You cry out for help!",
    target = "",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 89,
    name = "/hi",
    token = "HI",
    animation = true,
    voice = true,
    noTarget = "You greet everyone with a hearty hello!",
    target = "You greet <target> with a hearty hello!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 90,
    name = "/hug",
    token = "HUG",
    animation = false,
    voice = false,
    noTarget = "You need a hug!",
    target = "You hug <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 91,
    name = "/hungry",
    token = "HUNGRY",
    animation = false,
    voice = false,
    noTarget = "You are hungry!",
    target = "You are hungry. Maybe <target> has some food...",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 92,
    name = "/impatient",
    token = "IMPATIENT",
    animation = false,
    voice = false,
    noTarget = "You fidget.",
    target = "You fidget impatiently while waiting for <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 93,
    name = "/incoming",
    token = "INCOMING",
    animation = true,
    voice = true,
    noTarget = "You warn everyone of incoming enemies!",
    target = "You point out <target> as an incoming enemy!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 94,
    name = "/insult",
    token = "INSULT",
    animation = true,
    voice = false,
    noTarget = "You think everyone around you is a son of a motherless ogre.",
    target = "You think <target> is the son of a motherless ogre.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 95,
    name = "/introduce",
    token = "INTRODUCE",
    animation = false,
    voice = false,
    noTarget = "You introduce yourself to everyone.",
    target = "You introduce yourself to <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 96,
    name = "/jk",
    token = "JK",
    animation = false,
    voice = false,
    noTarget = "You were just kidding!",
    target = "You let <target> know that you were just kidding!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 97,
    name = "/kiss",
    token = "KISS",
    animation = true,
    voice = true,
    noTarget = "You blow a kiss into the wind.",
    target = "You blow a kiss to <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 98,
    name = "/kneel",
    token = "KNEEL",
    animation = true,
    voice = false,
    noTarget = "You kneel down.",
    target = "You kneel before <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 99,
    name = "/knuckles",
    token = "KNUCKLES",
    animation = false,
    voice = false,
    noTarget = "You crack your knuckles.",
    target = "You crack your knuckles while staring at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 100,
    name = "/laugh",
    token = "LAUGH",
    animation = true,
    voice = true,
    noTarget = "You laugh.",
    target = "You laugh at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 101,
    name = "/lavish",
    token = "LAVISH",
    animation = false,
    voice = false,
    noTarget = "You praise the Light.",
    target = "You lavish praise upon <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 102,
    name = "/lay", -- or /laydown
    token = "LAY",
    animation = true,
    voice = false,
    noTarget = "You lie down.",
    target = "You lie down before <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 103,
    name = "/lick",
    token = "LICK",
    animation = false,
    voice = false,
    noTarget = "You lick your lips.",
    target = "You lick <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 104,
    name = "/lie", -- or /liedown
    token = "LIE",
    animation = true,
    voice = false,
    noTarget = "You lie down.",
    target = "You lie down before <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 105,
    name = "/listen",
    token = "LISTEN",
    animation = false,
    voice = false,
    noTarget = "You are listening!",
    target = "You listen intently to <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 106,
    name = "/lol",
    token = "LOL",
    animation = true,
    voice = true,
    noTarget = "You laugh.",
    target = "You laugh at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 107,
    name = "/lost",
    token = "LOST",
    animation = true,
    voice = false,
    noTarget = "You are hopelessly lost.",
    target = "You want <target> to know that you are hopelessly lost.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 108,
    name = "/love",
    token = "LOVE",
    animation = false,
    voice = false,
    noTarget = "You feel the love.",
    target = "You love <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 109,
    name = "/mad",
    token = "MAD",
    animation = true,
    voice = false,
    noTarget = "You raise your fist in anger.",
    target = "You raise your fist in anger at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 110,
    name = "/massage",
    token = "MASSAGE",
    animation = false,
    voice = false,
    noTarget = "You need a massage!",
    target = "You massage <target>'s shoulders.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 111,
    name = "/moan",
    token = "MOAN",
    animation = false,
    voice = false,
    noTarget = "You moan suggestively.",
    target = "You moan suggestively at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 112,
    name = "/mock",
    token = "MOCK",
    animation = false,
    voice = false,
    noTarget = "You mock life and all it stands for.",
    target = "You mock the foolishness of <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 113,
    name = "/moo",
    token = "MOO",
    animation = false,
    voice = false, -- TODO: Tauren only
    noTarget = "Mooooooooooo.",
    target = "You moo at <target>. Mooooooooooo.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 114,
    name = "/moon",
    token = "MOON",
    animation = false,
    voice = false,
    noTarget = "You drop your trousers and moon everyone.",
    target = "You drop your trousers and moon <target>.",
    custom = false,
    customOrder = 0
  },
  { -- TODO NEED TO CHECK
    order = 115,
    name = "/mountspecial",
    token = "MOUNTSPECIAL",
    animation = true,
    voice = false,
    noTarget = "Rear-up animation for most mounts (including flying). Works only when mounted.",
    target = "",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 116,
    name = "/mourn",
    token = "MOURN",
    animation = true,
    voice = true,
    noTarget = "In quiet contemplation, you mourn the loss of the dead.",
    target = "In quiet contemplation, you mourn the death of <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 117,
    name = "/no",
    token = "NO",
    animation = true,
    voice = true,
    noTarget = "You clearly state, NO.",
    target = "You tell <target> NO. Not going to happen.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 118,
    name = "/nod",
    token = "NOD",
    animation = true,
    voice = true,
    noTarget = "You nod.",
    target = "You nod at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 119,
    name = "/nosepick",
    token = "NOSEPICK",
    animation = false,
    voice = false,
    noTarget = "With a finger deep in one nostril, you pass the time.",
    target = "You pick your nose and show it to <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 120,
    name = "/oom",
    token = "OOM",
    animation = true,
    voice = true,
    noTarget = "You announce that you have low mana!",
    target = "",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 121,
    name = "/openfire",
    token = "OPENFIRE",
    animation = true,
    voice = true,
    noTarget = "You give the order to open fire.",
    target = "",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 122,
    name = "/panic",
    token = "PANIC",
    animation = false,
    voice = false,
    noTarget = "You run around in a frenzied state of panic.",
    target = "You take one look at <target> and panic.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 123,
    name = "/pat",
    token = "PAT",
    animation = false,
    voice = false,
    noTarget = "You need a pat.",
    target = "You gently pat <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 124,
    name = "/peer",
    token = "PEER",
    animation = false,
    voice = false,
    noTarget = "You peer around, searchingly.",
    target = "You peer at <target> searchingly.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 125,
    name = "/peon",
    token = "PEON",
    animation = true,
    voice = false,
    noTarget = "You grovel on the ground, wallowing in subservience.",
    target = "You grovel before <target> like a subservient peon.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 126,
    name = "/pest",
    token = "PEST",
    animation = false,
    voice = false,
    noTarget = "You shoo the measly pests away.",
    target = "You shoo <target> away. Be gone pest!",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 127,
    name = "/pick",
    token = "PICK",
    animation = false,
    voice = false,
    noTarget = "With a finger deep in one nostril, you pass the time.",
    target = "You pick your nose and show it to <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 128,
    name = "/pity",
    token = "PITY",
    animation = false,
    voice = false,
    noTarget = "You pity those around you.",
    target = "You look down upon <target> with pity.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 129,
    name = "/pizza",
    token = "PIZZA",
    animation = false,
    voice = false,
    noTarget = "You are hungry!",
    target = "You are hungry. Maybe <target> has some food...",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 130,
    name = "/plead",
    token = "PLEAD",
    animation = true,
    voice = false,
    noTarget = "You drop to your knees and plead in desperation.",
    target = "You plead with <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 131,
    name = "/point",
    token = "POINT",
    animation = true,
    voice = false,
    noTarget = "You point over yonder.",
    target = "You point at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 132,
    name = "/poke",
    token = "POKE",
    animation = false,
    voice = false,
    noTarget = "You poke your belly and giggle.",
    target = "You poke <target>. Hey!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 133,
    name = "/ponder",
    token = "PONDER",
    animation = true,
    voice = false,
    noTarget = "You ponder the situation.",
    target = "You ponder <target>'s actions.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 134,
    name = "/pounce",
    token = "POUNCE",
    animation = false,
    voice = false,
    noTarget = "You pounce out from the shadows.",
    target = "You pounce on top of <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 135,
    name = "/praise",
    token = "PRAISE",
    animation = false,
    voice = false,
    noTarget = "You praise the Light.",
    target = "You lavish praise upon <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 136,
    name = "/pray",
    token = "PRAY",
    animation = true,
    voice = false,
    noTarget = "You pray to the Gods.",
    target = "You say a prayer for <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 137,
    name = "/purr",
    token = "PURR",
    animation = false,
    voice = false,
    noTarget = "You purr like a kitten.",
    target = "You purr at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 138,
    name = "/puzzled",
    token = "PUZZLED",
    animation = true,
    voice = false,
    noTarget = "You are puzzled. What's going on here?",
    target = "You are puzzled by <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 139,
    name = "/question",
    token = "QUESTION",
    animation = false,
    voice = false,
    noTarget = "You want to know the meaning of life.",
    target = "You question <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 140,
    name = "/raise",
    token = "RAISE",
    animation = false,
    voice = false,
    noTarget = "You raise your hand in the air.",
    target = "You look at <target> and raise your hand.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 141,
    name = "/rasp",
    token = "RASP",
    animation = true,
    voice = true,
    noTarget = "You make a rude gesture.",
    target = "You make a rude gesture at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 142,
    name = "/ready", -- or /rdy
    token = "READY",
    animation = false,
    voice = false,
    noTarget = "You let everyone know that you are ready!",
    target = "You let <target> know that you are ready!",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 143,
    name = "/rear",
    token = "REAR",
    animation = false,
    voice = false,
    noTarget = "You shake your rear.",
    target = "You shake your rear at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 144,
    name = "/roar",
    token = "ROAR",
    animation = true,
    voice = true,
    noTarget = "You roar with bestial vigor. So fierce!",
    target = "You roar with bestial vigor at <target>. So fierce!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 145,
    name = "/rofl",
    token = "ROFL",
    animation = true,
    voice = true,
    noTarget = "You roll on the floor laughing.",
    target = "You roll on the floor laughing at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 146,
    name = "/rude",
    token = "RUDE",
    animation = true,
    voice = true,
    noTarget = "You make a rude gesture.",
    target = "You make a rude gesture at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 147,
    name = "/salute",
    token = "SALUTE",
    animation = true,
    voice = false,
    noTarget = "You stand at attention and salute.",
    target = "You salute <target> with respect.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 148,
    name = "/scared",
    token = "SCARED",
    animation = false,
    voice = false,
    noTarget = "You are scared!",
    target = "You are scared of <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 149,
    name = "/scratch",
    token = "SCRATCH",
    animation = false,
    voice = false,
    noTarget = "You scratch yourself. Ah, much better!",
    target = "You scratch <target>. How catty!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 150,
    name = "/sexy",
    token = "SEXY",
    animation = false,
    voice = false,
    noTarget = "You're too sexy for your tunic...so sexy it hurts.",
    target = "You think <target> is a sexy devil.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 151,
    name = "/shake",
    token = "SHAKE",
    animation = false,
    voice = false,
    noTarget = "You shake your rear.",
    target = "You shake your rear at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 152,
    name = "/shimmy",
    token = "SHIMMY",
    animation = false,
    voice = false,
    noTarget = "You shimmy before the masses.",
    target = "You shimmy before <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 153,
    name = "/shindig",
    token = "SHINDIG",
    animation = true,
    voice = false,
    noTarget = "You raise a drink in the air before chugging it down. Cheers!",
    target = "You raise a drink to <target>. Cheers!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 154,
    name = "/shiver",
    token = "SHIVER",
    animation = false,
    voice = false,
    noTarget = "You shiver in your boots. Chilling!",
    target = "You shiver beside <target>. Chilling!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 155,
    name = "/shoo",
    token = "SHOO",
    animation = false,
    voice = false,
    noTarget = "You shoo the measly pests away.",
    target = "You shoo <target> away. Be gone pest!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 156,
    name = "/shrug",
    token = "SHRUG",
    animation = true,
    voice = false,
    noTarget = "You shrug. Who knows?",
    target = "You shrug at <target>. Who knows?",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 157,
    name = "/shy",
    token = "SHY",
    animation = true,
    voice = false,
    noTarget = "You smile shyly.",
    target = "You smile shyly at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 158,
    name = "/sigh",
    token = "SIGH",
    animation = false,
    voice = true,
    noTarget = "You let out a long, drawn-out sigh.",
    target = "You sigh at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 159,
    name = "/silly",
    token = "SILLY",
    animation = true,
    voice = true,
    noTarget = "You tell a joke.",
    target = "You tell <target> a joke.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 160,
    name = "/slap",
    token = "SLAP",
    animation = false,
    voice = false,
    noTarget = "You slap yourself across the face. Ouch!",
    target = "You slap <target> across the face. Ouch!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 161,
    name = "/sleep",
    token = "SLEEP",
    animation = true,
    voice = false,
    noTarget = "You fall asleep. Zzzzzzz.",
    target = "You fall asleep. Zzzzzzz.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 162,
    name = "/smell",
    token = "SMELL",
    animation = false,
    voice = false,
    noTarget = "You smell the air around you. Wow, someone stinks!",
    target = "You smell <target>. Wow, someone stinks!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 163,
    name = "/smile",
    token = "SMILE",
    animation = false,
    voice = false,
    noTarget = "You smile.",
    target = "You smile at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 164,
    name = "/smirk",
    token = "SMIRK",
    animation = false,
    voice = false,
    noTarget = "A sly smirk spreads across your face.",
    target = "You smirk slyly at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 165,
    name = "/snarl",
    token = "SNARL",
    animation = false,
    voice = false,
    noTarget = "You bare your teeth and snarl.",
    target = "You bare your teeth and snarl at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 166,
    name = "/snicker",
    token = "SNICKER",
    animation = false,
    voice = false,
    noTarget = "You quietly snicker to yourself.",
    target = "You snicker at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 167,
    name = "/sniff",
    token = "SNIFF",
    animation = false, -- Worgen only
    voice = false,
    noTarget = "You sniff the air around you.",
    target = "You sniff <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 168,
    name = "/snub",
    token = "SNUB",
    animation = false,
    voice = false,
    noTarget = "You snub all of the lowly peons around you.",
    target = "You snub <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 169,
    name = "/sob",
    token = "SOB",
    animation = true,
    voice = true,
    noTarget = "You cry.",
    target = "You cry on <target>'s shoulder.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 170,
    name = "/soothe",
    token = "SOOTHE",
    animation = false,
    voice = false,
    noTarget = "You need to be soothed.",
    target = "You soothe <target>. There, there...things will be ok.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 171,
    name = "/sorry",
    token = "SORRY",
    animation = false,
    voice = false,
    noTarget = "You apologize to everyone. Sorry!",
    target = "You apologize to <target>. Sorry!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 172,
    name = "/spit",
    token = "SPIT",
    animation = false,
    voice = false,
    noTarget = "You spit on the ground.",
    target = "You spit on <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 173,
    name = "/spoon",
    token = "SPOON",
    animation = false,
    voice = false,
    noTarget = "You need to be cuddled.",
    target = "You cuddle up against <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 174,
    name = "/stare",
    token = "STARE",
    animation = false,
    voice = false,
    noTarget = "You stare off into the distance.",
    target = "You stare <target> down.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 175,
    name = "/stink",
    token = "STINK",
    animation = false,
    voice = false,
    noTarget = "You smell the air around you. Wow, someone stinks!",
    target = "You smell <target>. Wow, someone stinks!",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 176,
    name = "/strong",
    token = "STRONG",
    animation = true,
    voice = true,
    noTarget = "You flex your muscles. Oooooh so strong!",
    target = "You flex at <target>. Oooooh so strong!",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 177,
    name = "/strut",
    token = "STRUT",
    animation = true,
    voice = true,
    noTarget = "With arms flapping, you strut around. Cluck, Cluck, Chicken!",
    target = "With arms flapping, you strut around <target>. Cluck, Cluck, Chicken!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 178,
    name = "/surprised",
    token = "SURPRISED",
    animation = false,
    voice = false,
    noTarget = "You are so surprised!",
    target = "You are surprised by <target>'s actions.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 179,
    name = "/surrender",
    token = "SURRENDER",
    animation = true,
    voice = false,
    noTarget = "You surrender to your opponents.",
    target = "You surrender before <target>. Such is the agony of defeat...",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 180,
    name = "/talk",
    token = "TALK",
    animation = true,
    voice = false,
    noTarget = "You talk to yourself since no one else seems interested",
    target = "You want to talk things over with <target>",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 181,
    name = "/talkex",
    token = "TALKEX",
    animation = true,
    voice = false,
    noTarget = "You talk excitedly with everyone",
    target = "You talk excitedly with <target>",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 182,
    name = "/talkq",
    token = "TALKQ",
    animation = true,
    voice = false,
    noTarget = "You want to know the meaning of life.",
    target = "You question <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 183,
    name = "/tap",
    token = "TAP",
    animation = false,
    voice = false,
    noTarget = "You tap your foot. Hurry up already!",
    target = "You tap your foot as you wait for <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 184,
    name = "/taunt",
    token = "TAUNT",
    animation = true,
    voice = true,
    noTarget = "You taunt everyone around you. Bring it fools!",
    target = "You make a taunting gesture at <target>. Bring it!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 185,
    name = "/tease",
    token = "TEASE",
    animation = false,
    voice = false,
    noTarget = "You are such a tease.",
    target = "You tease <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 186,
    name = "/thanks", -- or /thank
    token = "THANKS",
    animation = true,
    voice = true,
    noTarget = "You thank everyone around you.",
    target = "You thank <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 187,
    name = "/thirsty",
    token = "THIRSTY",
    animation = false,
    voice = false,
    noTarget = "You are so thirsty. Can anyone spare a drink?",
    target = "You let <target> know you are thirsty. Spare a drink?",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 188, -- NOTE: Addon classic threat meters overrides /threat
    name = "/threat", -- or /threaten or /doom
    token = "THREAT",
    animation = false,
    voice = false,
    noTarget = "You threaten everyone with the wrath of doom.",
    target = "You threaten <target> with the wrath of doom.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 189,
    name = "/tickle",
    token = "TICKLE",
    animation = false,
    voice = false,
    noTarget = "You want to be tickled. Hee hee!",
    target = "You tickle <target>. Hee hee!",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 190,
    name = "/tired",
    token = "TIRED",
    animation = false,
    voice = false,
    noTarget = "You let everyone know that you are tired.",
    target = "You let <target> know that you are tired.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 191,
    name = "/train",
    token = "TRAIN",
    animation = true,
    voice = true,
    noTarget = "\"Choo Choo Train\" animation and sound",
    target = "",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 192,
    name = "/ty",
    token = "TY",
    animation = true,
    voice = true,
    noTarget = "You thank everyone around you.",
    target = "You thank <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 193,
    name = "/veto",
    token = "VETO",
    animation = false,
    voice = false,
    noTarget = "You veto the motion on the floor.",
    target = "You veto <target>'s motion.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 194,
    name = "/victory",
    token = "VICTORY",
    animation = true,
    voice = false,
    noTarget = "You bask in the glory of victory.",
    target = "You bask in the glory of victory with <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 195,
    name = "/violin",
    token = "VIOLIN",
    animation = true,
    voice = true,
    noTarget = "You begin to play the world's smallest violin.",
    target = "You play the world's smallest violin for <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 196,
    name = "/volunteer",
    token = "VOLUNTEER",
    animation = false,
    voice = false,
    noTarget = "You raise your hand in the air.",
    target = "You look at <target> and raise your hand.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 197,
    name = "/wait",
    token = "WAIT",
    animation = true,
    voice = true,
    noTarget = "You ask everyone to wait.",
    target = "<target> asks <target> to wait.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 198,
    name = "/wave",
    token = "WAVE",
    animation = true,
    voice = false,
    noTarget = "You wave.",
    target = "You wave at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 199,
    name = "/weep",
    token = "WEEP",
    animation = true,
    voice = true,
    noTarget = "You cry.",
    target = "You cry on <target>'s shoulder.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 200,
    name = "/welcome",
    token = "WELCOME",
    animation = true,
    voice = true,
    noTarget = "You welcome everyone.",
    target = "You welcome <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 201,
    name = "/whine",
    token = "WHINE",
    animation = false,
    voice = false,
    noTarget = "You whine pathetically.",
    target = "You whine pathetically at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 203,
    name = "/whistle",
    token = "WHISTLE",
    animation = false,
    voice = true,
    noTarget = "You let forth a sharp whistle.",
    target = "You whistle at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 204,
    name = "/wicked", -- or /wickedly
    token = "WICKED",
    animation = false,
    voice = false,
    noTarget = "You grin wickedly.",
    target = "You grin wickedly at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 205,
    name = "/wink",
    token = "WINK",
    animation = false,
    voice = false,
    noTarget = "You wink slyly.",
    target = "You wink slyly at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 206,
    name = "/woot",
    token = "WOOT",
    animation = true,
    voice = true,
    noTarget = "You cheer!",
    target = "You cheer at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 207,
    name = "/work",
    token = "WORK",
    animation = false,
    voice = false,
    noTarget = "You begin to work.",
    target = "You work with <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 208,
    name = "/wrath",
    token = "WRATH",
    animation = false,
    voice = false,
    noTarget = "You threaten everyone with the wrath of doom.",
    target = "You threaten <target> with the wrath of doom.",
    custom = false,
    customOrder = 0
  },
  { -- verified
    order = 209,
    name = "/yawn",
    token = "YAWN",
    animation = false,
    voice = true,
    noTarget = "You yawn sleepily.",
    target = "You yawn sleepily at <target>.",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 210,
    name = "/yay",
    token = "YAY",
    animation = false,
    voice = false,
    noTarget = "You are filled with happiness!",
    target = "You are very happy with <target>!",
    custom = false,
    customOrder = 0
  },
  { -- DOES NOT WORK. NAME works but not TOKEN
    order = 211,
    name = "/yes",
    token = "YES",
    animation = true,
    voice = true,
    noTarget = "You nod.",
    target = "You nod at <target>.",
    custom = false,
    customOrder = 0
  }
}
