SEL = LibStub("AceAddon-3.0"):GetAddon("SEL", true) or LibStub("AceAddon-3.0"):NewAddon("SEL");
LibStub("AceAddon-3.0"):EmbedLibraries(SEL, "AceConsole-3.0", "AceEvent-3.0");

local addonName = ...;
SEL.addonName = addonName;
SEL.addonTitle = GetAddOnMetadata(addonName, "Title");
SEL.author	= GetAddOnMetadata(addonName, "Author");
SEL.version	= GetAddOnMetadata(addonName, "Version");
SEL.playerName =  UnitName("player");
SEL.playerLevel = UnitLevel("player");
-- SEL.container = nil;
-- SEL.event = nil;
-- SEL.group = nil;

SEL.about = {};
SEL.knownIssues = {};
SEL.changelog = {};
SEL.defaults = {};
SEL.options = {};
SEL.data = {};
